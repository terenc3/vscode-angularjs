# AngularJS Language Service for VS Code

This extentions contains [grammars](https://code.visualstudio.com/api/language-extensions/embedded-languages) and a [custom data](https://code.visualstudio.com/api/extension-guides/custom-data-extension) to aid in developing and maintaining [AngularJS](https://angularjs.org/) applications.

## Features

-   Completion for AngularJS tags and attributes
-   Hover for AngularJS tags and attributes
-   Syntax hightlighting in interpolated strings and AngularJS attributes

## Screenshots (Before/After)

![Before](./screenshots/before.png)
![After](./screenshots/after.png)
![Suggestions](./screenshots/suggestions.png)

## Custom Data

Collected from the latest [AngularJS API Docs](https://docs.angularjs.org/api) and the [source code](https://github.com/angular/angular.js/).

## Grammer

Modified the Grammars from [Angular Language Service Extension](https://github.com/angular/vscode-ng-language-service/tree/main/syntaxes) to work with AngularJS.

## Motivation

While there are multiple AngularJS extensions in the Marketplace and on GitHub, most of them only provide snippets. Writing a complete AngularJS Language Service would consume to much energy for a deprecated Framwork but using TypeScript, HTML entities and AngularJS grammar to hightlight interpolation and binding in Angular directives hits a sweet spot for me.

That also mean that it is unlike that i will invest much more time in this extension, but feel free to point out things which can quickly integrate to enhance the developer experience.

## Sources

-   Angular Logo taken from [Angular Press kit](https://angular.io/presskit#angularjs)
