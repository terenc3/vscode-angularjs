NODE := node
NPM := npm
BIN := ./node_modules/.bin
TSC := $(BIN)/tsc
VSCE := $(BIN)/vsce

BASE_URL := https://codeberg.org/terenc3/vscode-angularjs/raw/branch/main/

EXTENSION := $(shell ${NODE} -p "const {name, version} = require('./package.json'); [name, version].join('-')").vsix

SRC := $(wildcard src/*.ts)
OBJ = $(SRC:.ts=.js)

all: ${EXTENSION}

out/: ${SRC}
	$(TSC) -p tsconfig.json

%.vsix: out/
	$(VSCE) package --baseContentUrl $(BASE_URL)

watch:
	$(TSC) -p tsconfig.json -watch

clean:
	$(RM) -r out/
	$(RM) *.vsix

.PHONY: clean watch
